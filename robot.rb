class Robot
  def initialize(x:, y:, x_range:, y_range:, orientation:)
    @x = x
    @y = y
    @x_range = x_range
    @y_range = y_range
    @orientation = orientation
    @lost = !location_in_bounds?(x, y)
  end

  def process_instructions(instructions)
    instructions.chars.each do |i|
      break if @lost
      process_instruction(i)
    end
    self
  end

  def pretty_print
    "(#{@x}, #{@y}, #{@orientation})#{" LOST" if @lost}"
  end

  private

  ORIENTATION_MOVE_MAP = {
    "N" => { dx: 0, dy: 1 },
    "E" => { dx: 1, dy: 0 },
    "S" => { dx: 0, dy: -1 },
    "W" => { dx: -1, dy: 0 }
  }
  ORIENTATIONS = ORIENTATION_MOVE_MAP.keys

  MOVE = "F"
  ROTATION_INCREMENT_MAP = { "R" => 1, "L" => -1 }
  ROTATIONS = ROTATION_INCREMENT_MAP.keys

  def location_in_bounds?(x, y)
    @x_range.include?(x) && @y_range.include?(y)
  end

  def process_instruction(instruction)
    if instruction == MOVE
      move
    elsif ROTATIONS.include?(instruction)
      rotate(ROTATION_INCREMENT_MAP[instruction])
    else
      raise Exception.new "Invalid instruction"
    end
  end

  def move
    delta = move_delta
    new_x = @x + delta[:dx]
    new_y = @y + delta[:dy]

    if location_in_bounds?(new_x, new_y)
      @x = new_x
      @y = new_y
    else
      @lost = true
    end
  end

  def move_delta
    ORIENTATION_MOVE_MAP[@orientation] || (raise Exception.new "Invalid orientation")
  end

  def rotate(increment)
    new_orientation_index = (ORIENTATIONS.index(@orientation) + increment) % ORIENTATIONS.length
    @orientation = ORIENTATIONS[new_orientation_index]
  end
end
