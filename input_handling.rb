module InputHandling
  class << self
    def parse_input
      unless ARGV.empty?
        input_lines = IO.readlines(ARGV[0])
        validate_file(input_lines)
      else
        input_lines = fetch_user_input
      end

      dimension_line, *instruction_lines = input_lines

      {
        world_dimensions: parse_world_dimension_line(dimension_line),
        instruction_sets: instruction_lines.map { |l| parse_instruction_line(l) }
      }
    end

    private

    INVALID_INPUT_MESSAGE = "I didn't understand that. Please try again."

    def validate_file(input_lines)
      raise Exception.new "Input is empty" if input_lines.empty?

      dimensions, *instruction_lines = input_lines

      raise Exception.new "Invalid dimension line." unless valid_dimension_line?(dimensions)

      instruction_lines.each do |line|
        raise Exception.new "Invalid instrcution line #{line}" unless valid_instruction_line?(line)
      end
    end

    def fetch_user_input
      input_lines = [fetch_dimension_line].concat(fetch_instruction_lines)

      puts "Your input:"
      input_lines.each { |l| puts l }

      input_lines
    end

    def fetch_dimension_line
      while true
        puts "Input dimensions:"
        dimension_input = gets.strip

        break if valid_dimension_line?(dimension_input)
        puts INVALID_INPUT_MESSAGE
      end

      dimension_input
    end

    def fetch_instruction_lines
      instruction_lines = []
      while true
        puts "Are there more instruction lines? Y/N"
        response = gets.strip

        if response == "N"
          break
        elsif response == "Y"
          puts "Please input instruction line"
          instruction_line = gets.strip

          unless valid_instruction_line?(instruction_line)
            puts INVALID_INPUT_MESSAGE
            next
          end

          instruction_lines << instruction_line
        else
          puts "I only understand 'Y' or 'N'"
        end
      end

      instruction_lines
    end

    def valid_dimension_line?(input)
      input.match(/^\d+ \d+$/)
    end

    def valid_instruction_line?(input)
      input.match(/^\(-?\d+, -?\d+, [NESW]\) [FLR]*$/)
    end

    def parse_world_dimension_line(line)
      dimensions = line.split(" ").map(&:to_i)
      {
        width: dimensions.first,
        height: dimensions.last
      }
    end

    def parse_instruction_line(line)
      initial_state_string = line[/\(.+\)/]
      initial_state = initial_state_string.tr('() ', '').split(',')

      {
        initial_state: {
          x: initial_state[0].to_i,
          y: initial_state[1].to_i,
          orientation: initial_state[2]
        },
        instructions: line[/\).+/].tr(') ', '')
      }
    end
  end
end
