require "./input_handling"
require "./instruction_processing"

parsed_input = InputHandling.parse_input
output = InstructionProcessing.process(parsed_input)
puts "Final robot positions:"
puts output
