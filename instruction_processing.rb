require "./robot.rb"

module InstructionProcessing
  class << self
    def process(input)
      input[:instruction_sets].map do |instruction_set|
        Robot
          .new(
            x: instruction_set[:initial_state][:x],
            x_range: 0..(input[:world_dimensions][:width] - 1),
            y: instruction_set[:initial_state][:y],
            y_range: 0..(input[:world_dimensions][:height] - 1),
            orientation: instruction_set[:initial_state][:orientation]
          )
            .process_instructions(instruction_set[:instructions])
            .pretty_print
      end
        .join("\n")
    end
  end
end
