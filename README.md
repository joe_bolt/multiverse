`mars_rover.rb` is a ruby script implementing the algorithm for tracking the robots.
It can be run in two ways. It accepts a text file as an argument, e.g. `ruby mars_rover.rb test_string.txt` will run the algorithm on the sample input `test_string.txt`. It can also be run interactively without any input, e.g. `ruby mars_rover.rb`, in which case it will prompt the user for line by line input.

The next steps for improving the script would be to add unit tests, and also to add explicit command line flags (so the above examples would be `ruby mars_rover.rb --filename=test_string.txt` and `ruby mars_rover.rb -i` for the interactive mode)
